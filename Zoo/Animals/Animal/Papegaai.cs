﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals.Animal
{
    class Papegaai : IVogel
    {
        //JAVA manier van eigenschappen
        private String kleur;

        public void setKleur(String k)
        {
            kleur = k;
        }

        public String getKleur()
        {
            return kleur;
        }

        //C# methode
        //prop tab geeft property
        //property heeft een hoofdletter
        // set graag een magic variable: value
        public String Naam { get; set; }

        //_ gebruik je voor een private variable
        // meestal als onderliggende waarde voor een property
        private int _gewicht;
        public int Gewicht {
            get {return _gewicht;}
            set { _gewicht = value;}
        }

        public void Fly()
        {
            Console.WriteLine("Fly, fly, fly");
        }
    }
}
