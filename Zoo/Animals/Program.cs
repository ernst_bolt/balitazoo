﻿using Animals.Animal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    class Program
    {
        //Deze method, hiermee begint elke 
        //console app
        static void Main(string[] args)
        {
            //Maak een nieuwe papegaai =. INSTANTIE
            //Kan ie de class neit vinden => alt, shift + f10
            Papegaai piet = new Papegaai();
            piet.Fly();

            //blokhaken geeft aan dat het een array is.
            IVogel[] vogelverzameling = new IVogel[10];

            vogelverzameling[0] = piet;

            Console.ReadKey();
        }
    }
}
